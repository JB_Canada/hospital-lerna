import * as Hapi from 'hapi';
import * as PublicRoute from '@jberall-npm/public-route';
const server = new Hapi.Server({
  port: 3000,
  host: 'localhost'
});

const init = async () => {
  await server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      return 'Hello, world!';
    }
  });

  await server.register([
    {
      plugin: require('blipp')
    },
    { plugin: PublicRoute.plugin }
  ]);

  await server.route({
    method: 'GET',
    path: '/{name}',
    handler: (request, h) => {
      return 'Hello, ' + encodeURIComponent(request.params.name) + '!';
    }
  });
  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
  console.log(PublicRoute.plugin);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
