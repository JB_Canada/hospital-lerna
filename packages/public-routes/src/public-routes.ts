import * as Hapi from 'hapi';
function register(server: Hapi.Server) {
  console.log('Beginning Public Routes');

  server.route([
    {
      method: 'GET',
      path: '/public',
      handler: request => {
        return 'hello public routes';
      },
      options: {
        description: 'returns a Public.',
        validate: {}
      }
    }
  ]);
}
export const plugin = {
  name: 'Public Routes',
  register
};
